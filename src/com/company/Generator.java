package com.company;


import com.company.model.BlackMarket;
import com.company.model.Exchanger;
import com.company.model.Organization;

public class Generator {

    // protected static Bank GenereteBank() {

    // Bank bank = new Bank("Privat 24", 26.9f, 150_000);

    //  return bank;
    //}

    protected static Organization[] generate() {
        Organization[] organizations = new Organization[5];
        organizations[0] = new Bank("Privat 24", 25.9f, 150_000);
        organizations[1] = new Bank("Oshad", 28.6f, 126_000);
        organizations[2] = new Exchanger("Otr", 23.6f, 30_000);
        organizations[3] = new Exchanger("Lkj", 32.6f, 23_000);
        organizations[4] = new BlackMarket("BlackMarket", 28.5f);
        return organizations;
    }
    public static Bank getBank() {
        return new Bank("Oshad", 28.6f, 150_000);
    }

    public static BlackMarket getBlackMarket() {
        return new BlackMarket("BlackMarket", 28.5f);
    }

    public static Exchanger getExchanger() {
        return new Exchanger("Exchanger", 26.7f, 5_000);
    }
}





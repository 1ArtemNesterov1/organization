package com.company;

import com.company.model.Organization;

public class Bank extends Organization {

    private int limitInUah;

    public Bank(String name, float usd, int limit) {
        super(name, usd);
        this.limitInUah = limit;
    }

    public int getLimitInUah() {
        return limitInUah;
    }

    @Override
    public float exchange(int uah) {


      float usd =  super.exchange(uah);
        return uah < limitInUah ? usd : 0f;
    }

    //  if (uah < limitInUah) {
        //    return super.exchange(uah);
        //} else {
          //  return 0f;
        //}
    }


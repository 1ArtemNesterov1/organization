package com.company;

import com.company.model.BlackMarket;
import com.company.model.Exchanger;
import com.company.model.Organization;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("enter:");

        Scanner scanner = new Scanner(System.in);
        int put = Integer.parseInt(scanner.nextLine());




        Bank bank = Generator.getBank();
        BlackMarket blackMarket = Generator.getBlackMarket();
        Exchanger exchanger = Generator.getExchanger();


        Organization[] organizations = new Organization[5];
        organizations[0] = bank;

        organizations[1] = exchanger ;

        organizations[2] = blackMarket;

        for (Organization organization : organizations) {

            if (organization instanceof Exchanger){
                Exchanger exchanger1= (Exchanger) organization;
                exchanger1.getLimitInUsd();
            }
        }



        if (bank.exchange(put) > 0f) {
            System.out.println(String.format("in %s: %.2f usd", bank.getName(), bank.exchange(put)));
        }

        if (blackMarket.exchange(put) > 0f) {
            System.out.println(String.format("in blackMarket: %.2f usd", blackMarket.exchange(put)));
        }

        if (exchanger.exchange(put) > 0f) {
            System.out.println(String.format("in exchanger: %.2f usd", exchanger.exchange(put)));
        }

    }
}









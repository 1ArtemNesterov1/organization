package com.company.model;

import java.util.Objects;

public class Organization {
    protected String name;
    protected float raite;

    public Organization(String name, float raite) {
        this.name = name;
        this.raite = raite;
    }

    public String getName() {
        return name;
    }

    public float getRaite() {
        return raite;
    }

    public float exchange(int uah) {
        return uah / raite;
    }
}
